# This code is made and commented by Lucas Lasecla
# The purpose of this code is to take to numbers from the user and compare them
# and then display the greater of the two numbers




.data 		# beginning of setting data values
user1: 	.word	0		#creates a word called user1 holding value of 0
user2:	.word	0		#creates a word called user2 holding value of 0
userQ: 	.asciiz 	"Input a number(same number twice to exit): "	#creates string called userQ
ending:	.asciiz 	" is greater!" 		#creates a string called ending
newline:	.asciiz	"\n"		# creates a string that skips line
exit:	.asciiz		"Exiting...\n" 	# creates a string signifying end of code


.text	# beginning of code
.globl	main
main:  		# beginning of main
loop:		#loop jump point

	la 	$a0, userQ  # loads address of UserQ into $a0
	li $v0, 4    # prints string that is in $a0
    syscall		# executes the current system code
	li $v0, 5   # takes in an int from user and stores in $v0
    syscall		# executes the current system code
	move $t0, $v0		# moves the value currently in $v0 to $t0
	la 	$a0, newline  # loads address of newline into $a0
	li $v0, 4    # prints string that is in $a0
    syscall		# executes the current system code
	la 	$a0, userQ		# loads address of UserQ into $a0
	# prints string that is in $a0
    syscall		# executes the current system code
	li $v0, 5   # takes in an int from user and stores in $v0
    syscall		# executes the current system code
	move $t1, $v0		# moves the value currently in $v0 to $t1
    beq  $t0, $t1, end   # compares $t0 and $t1, if equal, jump to end
    blt  $t0, $t1, then	 # compares $t0 and $t1, if $t1 is greater, jump to then
# project requires greater int to be in $t2
    move $t2, $t0   # moves value in $t0 to $t2
    move $a0, $t2	# moves value in $t2 to $a0
    li	$v0, 1		# prints out the integer currently in $a0
	syscall		# executes the current system code
    la 	$a0, ending	# loads address of ending into $a0
	li $v0, 4    # prints string that is in $a0
    syscall		# executes the current system code
    la 	$a0, newline  # loads address of newLine into $a0
	li $v0, 4    # prints string that is in $a0
    syscall		# executes the current system code
    j loop  # jumps back up to the loop point
    
then:	# then jump point
# project requires greater int to be in $t2
    move $t2, $t1   # moves value in $t1 to $t2
    move $a0, $t2	# moves value in $t2 to $a0
    li	$v0, 1		# prints out the integer currently in $a0
	syscall		# executes the current system code
    la 	$a0, ending	# loads address of ending into $a0
	li $v0, 4    # prints string that is in $a0
    syscall		# executes the current system code
    la 	$a0, newline  # loads address of newLine into $a0
	li $v0, 4    # prints string that is in $a0
    syscall		# executes the current system code
    j loop  # jumps back up to the loop point
	
end: # end jump point
	la $a0, exit # loads address of exit string
	li $v0, 4    # prints string that is in $a0
    syscall		# executes the current system code
    li	$v0, 10		# system code to end the program
	syscall		# executes the current system code
.end main		# end of main
	