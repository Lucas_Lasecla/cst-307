 # This code is made and commented by Lucas Lasecla
# The purpose of this code is to take to a string from the user and determine if the 
# string is a palindrome or not


.data # section to set data values
string_space: .space 1024    # set aside 1024 bytes for the string.
userQ: 		.asciiz 	"Please enter string: "		# Prompt for user to enter a string
is:		.asciiz "The inputted string is a palindrome.\n" # the string is a palidrome
isNot:		.asciiz "The inputted string is not a palindrome.\n" # the string is not a palindrome
newline:	.asciiz	"\n"		# creates a string that skips line 
endQ: 		.asciiz "Enter 0 to enter another palindrome, otherwise programs ends...\n" #question at the end of the program
exit:	.asciiz		"Exiting...\n" 	# creates a string signifying end of code



.text	# section to code
main:                                    # SPIM starts by jumping to main.
                                            ## read the string S:
start:                           # start of code                 
	la 	$a0, userQ  # loads address of UserQ into $a0
	li $v0, 4    # prints string that is in $a0
	syscall		# executes the current system code
	la $a0, string_space	# loads in the buffer for the string
	li $a1, 1024		# communicates the size of the buffer
	li $v0, 8 # load "read_string" code into $v0.
	syscall		# executes the current system code
	la    $t1,      string_space                  # A = S.
	la    $t2, 		string_space                ## we need to move B to the end
length_loop:                                            # of the string:
    lb       $t3, ($t2)                          # load the byte at B into $t3.
    beqz  $t3, end_length_loop       # if $t3 == 0, branch out of loop.
    addu  $t2, $t2, 1                        # otherwise, increment B,
    j         length_loop                    # and repeat
end_length_loop:		# loop point for ending of the length loop
    subu   $t2, $t2, 2                     ## subtract 2 to move B back past
					     					# the ’\0’ and ’\n’.
                                                              
test_loop:  	# loop jump point for palindrome test
    bge $t1, $t2, is_palin               # if A >= B, it’s a palindrome.
    lb    $t3, ($t1)                           # load the byte at address A into $t3,
    lb    $t4, ($t2)                           # load the byte at address B into $t4.
    bne $t3, $t4, not_palin            # if $t3 != $t4, not a palindrome.
                                                    # Otherwise,
    addu $t1, $t1, 1                       # increment A,
    subu $t2, $t2, 1                       # decrement B,
    j       test_loop                        # and repeat the loop.
    
is_palin:  # jump point for if the word is a palindrome
	la $a0, is	# loads in the string to say that the word is a palindrome
	li $v0, 4    # prints string that is in $a0
	syscall		# executes the current system code
	j end 		# jumps down to the end point
	
not_palin:		# jump point for if the word is not a palindrome
	la $a0, isNot	# loads in the string to say that the word is not a palindrome
	li $v0, 4    # prints string that is in $a0
	syscall		# executes the current system code
	j end		# jumps down to the end point
                                                              

end: 	# end jump point
	la 	$a0, endQ  # loads address of endQ into $a0
	li $v0, 4    # prints string that is in $a0
	syscall		# executes the current system code
	li $v0, 5   # takes in an int from user and stores in $v0
    syscall		# executes the current system code
	move $t6, $v0		# moves the value currently in $v0 to $t0
	beqz  $t6, start       # if $t3 == 0, branch out of loop.
	la 	$a0, exit  # loads address of exit into $a0
	li $v0, 4    # prints string that is in $a0
    syscall		# executes the current system code
    li	$v0, 10		# system code to end the program
	syscall		# executes the current system code
.end main		# end of main
