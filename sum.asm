# This code is made and commented by Lucas Lasecla
# The purpose of this code is to take to numbers from the user and add them together
# and then display the sum




.data 		# beginning of setting data values
user1: 	.word	0		#creates a word called user1 holding value of 0
user2:	.word	0		#creates a word called user2 holding value of 0
userQ: 	.asciiz 	"Input a number: "	#creates string called userQ
sum: 	.asciiz 	"The sum is: "		# creates a string presenting the sum
newline:	.asciiz	"\n"		# creates a string that skips line
exit:	.asciiz		"Exiting...\n" 	# creates a string signifying end of code



.text	# beginning of code
.globl	main
main:  		# beginning of main

	la 	$a0, userQ  # loads address of UserQ into $a0
	li $v0, 4    # prints string that is in $a0
    syscall		# executes the current system code
	li $v0, 5   # takes in an int from user and stores in $v0
    syscall		# executes the current system code
	move $t0, $v0		# moves the value currently in $v0 to $t0
	la 	$a0, newline  # loads address of newline into $a0
	li $v0, 4    # prints string that is in $a0
    syscall		# executes the current system code
	la 	$a0, userQ		# loads address of UserQ into $a0
	li $v0, 4 # prints string that is in $a0
    syscall		# executes the current system code
	li $v0, 5   # takes in an int from user and stores in $v0
    syscall		# executes the current system code
	move $t1, $v0		# moves the value currently in $v0 to $t1
	add $t2, $t1, $t0	# adds the values in $t1 and $t0, sum put into $t2
	la 	$a0, sum  # loads address of sum into $a0
	li $v0, 4    # prints string that is in $a0
    syscall		# executes the current system code
    move $a0, $t2 	# moves value in $t2 to $a0
    li	$v0, 1		# prints out the integer currently in $a0
	syscall		# executes the current system code
	la 	$a0, newline  # loads address of newline into $a0
	li $v0, 4    # prints string that is in $a0
    syscall		# executes the current system code
	la 	$a0, exit  # loads address of exit into $a0
	li $v0, 4    # prints string that is in $a0
    syscall		# executes the current system code
    li	$v0, 10		# system code to end the program
	syscall		# executes the current system code
.end main		# end of main
